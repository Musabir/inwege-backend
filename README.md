# InWeGe back-end

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Generally the application provides APIs which are for showing salary for various regions and occupations.

# Features!

  - Parse .xlxs file and load to database.
  - Get salary for a specific occupation and region.
  - Get average salary for whole region and country.


### Requirements

For building and running the application it is needed to have:

* [Maven 3](https://maven.apache.org/) - Dependency Management.
* [JDK 1.8](https://www.java.com/en/) - Java™ Platform, Standard Edition Development Kit
* [PostgreSQL](https://www.postgresql.org/) - Relational database management system

### Installation & Running
The application is running on port: ``` 8083```. It can be changed from ```application.properties``` file by alering the line:

```server.port=[any port number]```

There are several ways to run a Spring Boot application on your local machine. One way is to execute the main method in the ``` com.inwege.inwege.InwegeApplication ```
class from your IDE.

  - Download the zip or clone the Git repository.
  - Unzip the zip file (if you downloaded one)
  - Open Command Prompt and Change directory (cd) to folder containing pom.xml
  - Open IntelliJ
  - File -> Open ->  Navigate to the folder where you unzipped the zip
  - Select the project
  - Choose the folder
  - Right Click on the file and Run as Java Application

Alternatively you can use the Spring Boot Maven plugin like so:

  - First create .war file

    ```
        $ mvn clean package
      ```

  - Run the .war file 
  - 
    ```
        $ java -war  InWege-backend/target/inwege-0.0.1-SNAPSHOT.war
    ```

After successfully running, the application can be checked from the following link:
```sh
$ http://localhost:8083   
```
### Running tests
The tests can be run by the following command:
```sh
$ mvn test
```


#### Actuator


| URL        | Method           | Comment  |
| ------------- |:-------------:| -----:|
| ```http://localhost:80/api/regions ```  | GET | Get all regions |
| ```http://localhost:80/api/average?region={regionName}```  | GET | Get average of salary by region|
| ```http://localhost:80/api/jobs/names?region={regionName}&lang={lang} ```  | GET | Get occupations for a region  |
| ```http://localhost:80/api/jobs?region={regionName}&lang={lang}&isco={iscoValid}&code={code}```  | GET | Get salary and job details for a specific occupation|
| ```http://localhost:80/api/jobs/{jobId}/average```  | GET |Get average for a specific occupation|
| ```http://localhost:80/api/salary-entities?region={regionName}&isco={isco}```  | GET |Get salary details for a specific occupation in a specific region|


### Todos

 - Developing Admin Panel APIs


package com.inwege.inwege.controller;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inwege.inwege.InwegeApplication;
import com.inwege.inwege.common.application.dto.ResponseDTO;
import com.inwege.inwege.common.application.exception.JobEntityNotFoundException;
import com.inwege.inwege.common.application.exception.SalaryEntityNotFoundException;
import com.inwege.inwege.databinder.application.domain.model.SalaryEntity;
import com.inwege.inwege.databinder.application.domain.model.SalaryEntityAverage;
import com.inwege.inwege.databinder.application.domain.repository.SalaryEntityRepository;
import com.inwege.inwege.databinder.application.domain.service.EntityService;
import com.inwege.inwege.databinder.application.dto.AverageMeanDTO;
import com.inwege.inwege.databinder.application.dto.JobAndSalaryEntitiesDTO;
import com.inwege.inwege.databinder.application.dto.JobNameDTO;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.print.attribute.standard.JobName;
import javax.swing.text.html.parser.Entity;
import javax.transaction.Transactional;

import java.lang.reflect.Array;
import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = InwegeApplication.class)
@WebAppConfiguration
@DirtiesContext
@Transactional
public class MainControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    SalaryEntityRepository salaryEntityRepository;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void findAllRegionsTest() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/regions")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO regions =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(regions.getCode()).isEqualTo(200);
        assertThat(regions.getStatus()).isEqualTo("success");
        assertThat(((List<String>) regions.getPayload()).size()).isGreaterThan(0);
    }

    @Test
    public void findAverageMeanTest() throws Exception {
        String validId = "";
        if (!salaryEntityRepository.findAll().isEmpty())
            validId = salaryEntityRepository.findAll().get(0).getId() + "";
        String wrongId = "212211200001";
        MvcResult result = mockMvc.perform(get("/api/jobs/" + validId + "/average")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO.getCode()).isEqualTo(200);
        assertThat(responseDTO.getStatus()).isEqualTo("success");
        List<AverageMeanDTO> averageMeanDTOS = mapper.convertValue(responseDTO.getPayload(), new TypeReference<List<AverageMeanDTO>>() {
        });
        assertThat(averageMeanDTOS.size()).isEqualTo(2);
        assertThat(averageMeanDTOS.get(0).getFemale()).isEqualTo(0);
        assertThat(averageMeanDTOS.get(1).getFemale()).isEqualTo(1);
        assertThat(averageMeanDTOS.get(0).getMean()).isGreaterThan(0);
        assertThat(averageMeanDTOS.get(1).getMean()).isGreaterThan(0);


        result = mockMvc.perform(get("/api/jobs/" + wrongId + "/average")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        responseDTO =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO.getCode()).isEqualTo(404);
        assertThat(responseDTO.getStatus()).isEqualTo("success");
        assertThat(responseDTO.getPayload()).isEqualTo(new ArrayList<>());
    }

    @Test
    public void findSalaryEntryByRegionAndIscoTest() throws Exception {
        String iscoValid = "3150";
        String wrongIscoValid = "0001013150";
        String region = "Tallinn";
        String wrongRegion = "Baku";
        MvcResult result = mockMvc.perform(get("/api/salary-entities")
                .param("isco", iscoValid)
                .param("region", region)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO.getCode()).isEqualTo(200);
        assertThat(responseDTO.getStatus()).isEqualTo("success");
        SalaryEntity salaryEntity = mapper.convertValue(responseDTO.getPayload(), new TypeReference<SalaryEntity>() {
        });

        assertThat(salaryEntity.getRegion()).isEqualTo(region);
        assertThat(salaryEntity.getIscoValid()).isEqualTo(iscoValid);


        result = mockMvc.perform(get("/api/salary-entities")
                .param("isco", wrongIscoValid)
                .param("region", region)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        responseDTO =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO.getCode()).isEqualTo(200);
        assertThat(responseDTO.getStatus()).isEqualTo("success");
        assertThat(responseDTO.getPayload()).isEqualTo(null);


        result = mockMvc.perform(get("/api/salary-entities")
                .param("isco", iscoValid)
                .param("region", wrongRegion)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        responseDTO =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO.getCode()).isEqualTo(200);
        assertThat(responseDTO.getStatus()).isEqualTo("success");
        assertThat(responseDTO.getPayload()).isEqualTo(null);
    }

    @Test
    public void findJobNamesByRegionTest() throws Exception {
        String lang_en = "en";
        String lang_ee = "ee";
        String region = "Tallinn";
        String wrongRegion = "Baku";
        MvcResult result = mockMvc.perform(get("/api/jobs/names")
                .param("lang", lang_en)
                .param("region", region)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO_en =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO_en.getCode()).isEqualTo(200);
        assertThat(responseDTO_en.getStatus()).isEqualTo("success");
        List<JobNameDTO> jobNameDTOS_en = mapper.convertValue(responseDTO_en.getPayload(), new TypeReference<List<JobNameDTO>>() {
        });

        assertThat(jobNameDTOS_en.size()).isGreaterThan(0);
        Set<JobNameDTO> distinctJobNames = new HashSet<JobNameDTO>(jobNameDTOS_en);
        assertThat(jobNameDTOS_en.size()).isEqualTo(distinctJobNames.size());

        result = mockMvc.perform(get("/api/jobs/names")
                .param("lang", lang_ee)
                .param("region", region)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO_ee =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO_ee.getCode()).isEqualTo(200);
        assertThat(responseDTO_ee.getStatus()).isEqualTo("success");
        List<JobNameDTO> jobNameDTOS_ee = mapper.convertValue(responseDTO_ee.getPayload(), new TypeReference<List<JobNameDTO>>() {
        });

        assertThat(jobNameDTOS_ee.size()).isGreaterThan(0);
        assertThat(jobNameDTOS_ee).isNotEqualTo(jobNameDTOS_en);
        assertThat(jobNameDTOS_ee.size()).isEqualTo(jobNameDTOS_en.size());

        result = mockMvc.perform(get("/api/jobs/names")
                .param("lang", lang_ee)
                .param("region", wrongRegion)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO_wrong =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO_wrong.getCode()).isEqualTo(404);
        assertThat(responseDTO_wrong.getStatus()).isEqualTo("success");
        assertThat(responseDTO_wrong.getPayload()).isEqualTo(new ArrayList<>());

    }

    @Test
    public void findJobEntityByCodeTest() throws Exception {
        String lang = "en";
        String region = "Tallinn";
        String isco = "3150";
        String code = "315";
        String wrongRegion = "Baku";
        MvcResult result = mockMvc.perform(get("/api/jobs")
                .param("lang", lang)
                .param("isco", isco)
                .param("code", code)
                .param("region", region)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO_en =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO_en.getCode()).isEqualTo(200);
        assertThat(responseDTO_en.getStatus()).isEqualTo("success");
        JobAndSalaryEntitiesDTO jobAndSalaryEntitiesDTO = mapper.convertValue(responseDTO_en.getPayload(), new TypeReference<JobAndSalaryEntitiesDTO>() {
        });

        assertThat(jobAndSalaryEntitiesDTO.getJobEntity().getCode()).isEqualTo(code);
        assertThat(jobAndSalaryEntitiesDTO.getSalaryEntities().get(0).getIscoValid()).isEqualTo(isco);
        assertThat(jobAndSalaryEntitiesDTO.getSalaryEntities().get(0).getRegion()).isEqualTo(region);
        assertThat(jobAndSalaryEntitiesDTO.getSalaryEntities().get(0).getOneDigit())
                .isEqualTo(jobAndSalaryEntitiesDTO.getSalaryEntities().get(1).getOneDigit());
        assertThat(jobAndSalaryEntitiesDTO.getSalaryEntities().get(0).getTwoDigit())
                .isEqualTo(jobAndSalaryEntitiesDTO.getSalaryEntities().get(1).getTwoDigit());
        assertThat(jobAndSalaryEntitiesDTO.getSalaryEntities().get(0).getThreeDigit())
                .isEqualTo(jobAndSalaryEntitiesDTO.getSalaryEntities().get(1).getThreeDigit());
        assertThat(jobAndSalaryEntitiesDTO.getSalaryEntities().get(0).getFourDigit())
                .isEqualTo(jobAndSalaryEntitiesDTO.getSalaryEntities().get(1).getFourDigit());


        result = mockMvc.perform(get("/api/jobs")
                .param("lang", lang)
                .param("isco", isco)
                .param("code", code)
                .param("region", wrongRegion)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO_wrong =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO_wrong.getStatus()).isEqualTo("success");

    }

    @Test
    public void findAverageByRegion() throws Exception {
        String region = "Tallinn";
        String wholeEstonia = "All";
        String wrongRegion = "Baku";
        MvcResult result = mockMvc.perform(get("/api/average")
                .param("region", region)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });

        assertThat(responseDTO.getCode()).isEqualTo(200);
        assertThat(responseDTO.getStatus()).isEqualTo("success");

        result = mockMvc.perform(get("/api/average")
                .param("region", wholeEstonia)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO_all =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO_all.getCode()).isEqualTo(200);
        assertThat(responseDTO_all.getStatus()).isEqualTo("success");

        result = mockMvc.perform(get("/api/average")
                .param("region", wrongRegion)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ResponseDTO responseDTO_wrong =
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<ResponseDTO>() {
                        });
        assertThat(responseDTO_wrong.getCode()).isEqualTo(404);
        assertThat(responseDTO_wrong.getPayload()).isEqualTo(new ArrayList<>());

    }
}

package com.inwege.inwege.repositories;

import com.inwege.inwege.InwegeApplication;
import com.inwege.inwege.databinder.application.domain.model.JobEntity;
import com.inwege.inwege.databinder.application.domain.repository.JobEntityRepository;
import com.inwege.inwege.databinder.application.domain.repository.SalaryEntityRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = InwegeApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class JobEntityRepositoryTest {

    @Autowired
    JobEntityRepository jobEntityRepository;

    @Autowired
    SalaryEntityRepository salaryEntityRepository;


    @Test
    public void findJobEntityByCodeTest() {
        List<JobEntity> jobEntities = jobEntityRepository.findJobEntityByCode("0");
        assertThat(jobEntities.size()).isLessThan(2);
        assertThat(jobEntities.size()).isGreaterThan(0);
        assertThat(jobEntities.get(0).getId()).isNotNull();
        assertThat(jobEntities.get(0)).isNotNull();

        List<JobEntity> jobEntityFailed = jobEntityRepository.findJobEntityByCode("-232");
        assertThat(jobEntityFailed).isEmpty();

    }

    @Test
    public void findNameEeTest() {
        List<String> namesEe = jobEntityRepository.findAllNameEe();
        assertThat(namesEe.size()).isGreaterThan(0);
    }

    @Test
    public void findNameEnTest() {
        List<String> namesEn = jobEntityRepository.findAllNameEn();
        assertThat(namesEn.size()).isGreaterThan(0);
    }

    @Test
    public void findNameRuTest() {
        List<String> namesRu = jobEntityRepository.findAllNameRu();
        assertThat(namesRu.size()).isGreaterThan(0);
    }

    @Test
    public void generalNameTest() {
        List<String> namesRu = jobEntityRepository.findAllNameRu();
        List<String> namesEn = jobEntityRepository.findAllNameEn();
        List<String> namesEe = jobEntityRepository.findAllNameEe();
        assertThat(namesRu).isNotEqualTo(namesEe);
        assertThat(namesRu).isNotEqualTo(namesEn);
        assertThat(namesEe).isNotEqualTo(namesEn);
    }
}

package com.inwege.inwege.repositories;


import com.inwege.inwege.InwegeApplication;
import com.inwege.inwege.databinder.application.domain.model.SalaryEntity;
import com.inwege.inwege.databinder.application.domain.repository.JobEntityRepository;
import com.inwege.inwege.databinder.application.domain.repository.SalaryEntityRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = InwegeApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SalaryEntityRepositoryTest {

    @Autowired
    JobEntityRepository jobEntityRepository;

    @Autowired
    SalaryEntityRepository salaryEntityRepository;

    @Test
    public void findIscosTest() {
        List<String> iscosList = salaryEntityRepository.findAllDistictRegions();
        assertThat(iscosList.size()).isGreaterThan(0);

        Set<String> iscosSet = new HashSet<String>(iscosList);
        assertThat(iscosSet.size()).isEqualTo(iscosList.size());
    }

    @Test
    public void findIscosByRegionTest() {
        List<SalaryEntity> salaryEntities = salaryEntityRepository.findIscosByRegion("Tallinn");
        assertThat(salaryEntities.size()).isGreaterThan(0);

        assertThat(salaryEntityRepository.findIscosByRegion("Baku").size()).isEqualTo(0);

        Set<SalaryEntity> salaryEntityHashSet = new HashSet<SalaryEntity>(salaryEntities);
        assertThat(salaryEntityHashSet.size()).isEqualTo(salaryEntities.size());
    }

    @Test
    public void findSalaryEntitiesByRegionAndIscosTest() {
        List<SalaryEntity> salaryEntitiesList = salaryEntityRepository.findSalaryEntriesByRegionAndIsco("Tallinn", "1000");
        assertThat(salaryEntitiesList.size()).isEqualTo(2);

        List<SalaryEntity> wrongIscoList = salaryEntityRepository.findSalaryEntriesByRegionAndIsco("Tallinn", "10001121221212");
        assertThat(wrongIscoList.size()).isEqualTo(0);

        List<SalaryEntity> wrongRegionList = salaryEntityRepository.findSalaryEntriesByRegionAndIsco("TallinnBaku", "1000");
        assertThat(wrongRegionList.size()).isEqualTo(0);

        List<SalaryEntity> emptyRegionList = salaryEntityRepository.findSalaryEntriesByRegionAndIsco("", "1000");
        assertThat(emptyRegionList.size()).isEqualTo(0);

        List<SalaryEntity> emptyIscoList = salaryEntityRepository.findSalaryEntriesByRegionAndIsco("Tallinn", "");
        assertThat(emptyIscoList.size()).isEqualTo(0);

    }

    @Test
    public void findSalaryEntitiesByIscoValidTest() {
        List<SalaryEntity> salaryEntities = salaryEntityRepository.findSalaryEntitiesByIscoValid("1000");

        assertThat(salaryEntities.size()).isGreaterThan(0);
        assertThat(salaryEntities.get(0).getIscoValid()).isEqualTo(salaryEntities.get(salaryEntities.size() - 1).getIscoValid());


        assertThat(salaryEntityRepository.findSalaryEntitiesByIscoValid("10121212").size()).isEqualTo(0);
    }

    @Test
    public void salaryEntitiesTest() {
        List<SalaryEntity> salaryEntities = salaryEntityRepository.findAll();
        SalaryEntity salaryEntity = getRandomElement(salaryEntities);
        System.out.println(salaryEntity.getId());
        assertThat(Double.parseDouble(salaryEntity.getP9())).isLessThanOrEqualTo(Double.parseDouble(salaryEntity.getMaxVal()));
        assertThat(Double.parseDouble(salaryEntity.getMinVal())).isLessThanOrEqualTo(Double.parseDouble(salaryEntity.getP1()));
        assertThat(Double.parseDouble(salaryEntity.getN())).isGreaterThan(0);
        int sum = Integer.parseInt(salaryEntity.getOneDigit()) + Integer.parseInt(salaryEntity.getTwoDigit()) +
                Integer.parseInt(salaryEntity.getThreeDigit()) + Integer.parseInt(salaryEntity.getFourDigit());
        assertThat(sum).isLessThanOrEqualTo(4);
        assertThat(sum).isGreaterThan(0);
    }

    private SalaryEntity getRandomElement(List<SalaryEntity> list)
    {
        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }
}

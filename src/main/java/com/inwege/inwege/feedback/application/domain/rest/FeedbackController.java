package com.inwege.inwege.feedback.application.domain.rest;

import com.inwege.inwege.common.application.dto.ResponseDTO;
import com.inwege.inwege.common.application.dto.ResponseStatus;
import com.inwege.inwege.common.application.exception.FeedbackNotFoundException;
import com.inwege.inwege.feedback.application.domain.service.FeedbackService;
import com.inwege.inwege.feedback.application.dto.FeedbackDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class FeedbackController {

    @Autowired
    FeedbackService feedbackService;

    @PostMapping("/feedback")
    public ResponseDTO<?> createFeedback(@RequestBody FeedbackDTO feedbackDTO) {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.CREATED.value(),
                "Feedback created", feedbackService.createFeedback(feedbackDTO));

    }

    @GetMapping("/feedbacks")
    public ResponseDTO<?> getAllFeedback() {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.OK.value(),
                "All feedback ", feedbackService.getFeedbacks());
    }

    @GetMapping("/feedbacks/{id}")
    public ResponseDTO<?> getFeedbackById(@PathVariable Long id) throws FeedbackNotFoundException {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.OK.value(),
                "Feedback by id: " + id, feedbackService.getFeeback(id));
    }


    @GetMapping("/feedback")
    public ResponseDTO<?> getFeedbackByEmail(@RequestParam String email) {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.OK.value(),
                "Feedback by email: " + email, feedbackService.getFeedbackByEmail(email));
    }

    @ExceptionHandler(FeedbackNotFoundException.class)
    public ResponseDTO handleFeedbackNotFoundException(FeedbackNotFoundException ex) {
        return new ResponseDTO(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), new ArrayList<>());
    }


}

package com.inwege.inwege.feedback.application.dto;

import lombok.Data;

@Data
public class FeedbackDTO {

    Long id;
    String email;
    String details;
    String description;

    public FeedbackDTO(Long id, String email, String details, String description) {
        this.id = id;
        this.email = email;
        this.details = details;
        this.description = description;
    }

    public FeedbackDTO() {

    }
}

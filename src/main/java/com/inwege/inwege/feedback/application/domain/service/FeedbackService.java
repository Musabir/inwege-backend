package com.inwege.inwege.feedback.application.domain.service;

import com.inwege.inwege.common.application.domain.service.EmailService;
import com.inwege.inwege.common.application.exception.FeedbackNotFoundException;
import com.inwege.inwege.feedback.application.domain.model.Feedback;
import com.inwege.inwege.feedback.application.domain.repository.FeedbackRepository;
import com.inwege.inwege.feedback.application.dto.FeedbackDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackService {

    @Autowired
    FeedbackRepository feedbackRepository;

    @Autowired
    EmailService emailService;

    public Feedback createFeedback(FeedbackDTO feedbackDTO) {
        Feedback feedback = new Feedback();
        feedback.setEmail(feedbackDTO.getEmail());
        feedback.setDescription(feedbackDTO.getDescription());
        feedback.setDetails(feedbackDTO.getDetails());
        emailService.sendMail(feedbackDTO.getEmail());
        return feedbackRepository.save(feedback);
    }

    public List<Feedback> getFeedbacks() {
        return feedbackRepository.findAll();
    }

    public List<Feedback> getFeedbackByEmail(String email) {
        return feedbackRepository.findFeedbackByEmail(email);
    }

    public Feedback getFeeback(Long id) throws FeedbackNotFoundException {
        Feedback feedback = feedbackRepository.findById(id).orElse(null);
        if(feedback==null)
            throw new FeedbackNotFoundException(id);
        return feedback;
    }
}

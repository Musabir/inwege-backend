package com.inwege.inwege.feedback.application.domain.repository;

import com.inwege.inwege.feedback.application.domain.model.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, Long> {

    List<Feedback> findFeedbackByEmail(String email);
}

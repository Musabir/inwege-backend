package com.inwege.inwege.feedback.application.domain.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Feedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String email;

    @Column( length = 100000)
    private String description;

    @Column( length = 100000)
    private String details;
}

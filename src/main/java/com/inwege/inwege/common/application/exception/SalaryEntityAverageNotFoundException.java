package com.inwege.inwege.common.application.exception;

public class SalaryEntityAverageNotFoundException extends Exception{
    public SalaryEntityAverageNotFoundException(String message) {
        super(message);
    }
}

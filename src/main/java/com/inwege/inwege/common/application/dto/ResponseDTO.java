package com.inwege.inwege.common.application.dto;

import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

@Data
public class ResponseDTO<T>  {
    String status;
    int code;
    String message;
    T payload;

    public ResponseDTO(String status, int code, String message, T payload) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.payload = payload;
    }
}

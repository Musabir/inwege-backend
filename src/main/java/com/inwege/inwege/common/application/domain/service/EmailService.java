package com.inwege.inwege.common.application.domain.service;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {


    private JavaMailSender javaMailSender;

    public EmailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMail(String toEmail) {

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        String msg = "Hello,\nThank you for the feedback! \n\nBest regards,\nInWeGe Team";
        mailMessage.setTo(toEmail);
        mailMessage.setSubject("Feedback for InWeGe application");
        mailMessage.setText(msg);

        javaMailSender.send(mailMessage);
    }
}

package com.inwege.inwege.common.application.exception;

public class FeedbackNotFoundException extends Exception{
    public FeedbackNotFoundException(String message) {
        super(message);
    }
    public FeedbackNotFoundException(Long  id) {
        super("No feedback found with the given id: "+ id);
    }
}

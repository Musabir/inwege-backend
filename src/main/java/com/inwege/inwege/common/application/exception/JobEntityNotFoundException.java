package com.inwege.inwege.common.application.exception;

public class JobEntityNotFoundException extends Exception{
    public JobEntityNotFoundException(String message) {
        super(message);
    }
}

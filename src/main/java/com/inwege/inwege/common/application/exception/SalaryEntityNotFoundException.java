package com.inwege.inwege.common.application.exception;

public class SalaryEntityNotFoundException extends Exception{
    public SalaryEntityNotFoundException(String message) {
        super(message);
    }
}

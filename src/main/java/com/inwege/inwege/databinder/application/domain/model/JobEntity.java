package com.inwege.inwege.databinder.application.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;

@Data
@Entity
public class JobEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column( length = 100000 )
    private String code, level, name_ee, name_en, name_ru, description_ee, description_en, description_ru, belong_group_ee, belong_group_en, belong_group_ru,
    not_belong_group_ee,not_belong_group_en,not_belong_group_ru,start_date;

    public JobEntity(ArrayList<String> list) {
        code = list.get(0);
        level = list.get(1);
        name_ee = list.get(2);
        name_en = list.get(3);
        name_ru = list.get(4);
        description_ee = list.get(5);
        description_en = list.get(6);
        description_ru = list.get(7);
        belong_group_ee = list.get(8);
        belong_group_en = list.get(9);
        belong_group_ru = list.get(10);
        not_belong_group_ee = list.get(11);
        not_belong_group_en = list.get(12);
        not_belong_group_ru = list.get(13);
        start_date = list.get(14);
    }

    public JobEntity(){ }
}

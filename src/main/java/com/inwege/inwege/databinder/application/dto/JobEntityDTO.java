package com.inwege.inwege.databinder.application.dto;

import lombok.Data;

@Data
public class JobEntityDTO {
    Long id;
    String name;
    String code;
    String description;
    String belong_group;
    String not_belong_group;

    public JobEntityDTO(Long id, String name, String code, String description, String belong_group, String not_belong_group) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.description = description;
        this.belong_group = belong_group;
        this.not_belong_group = not_belong_group;
    }
    public JobEntityDTO(){}


}

package com.inwege.inwege.databinder.application.dto;

public class AverageMeanDTO {
    double mean;
    int female;

    public AverageMeanDTO(double mean, int female) {
        this.mean = mean;
        this.female = female;
    }

    public double getMean() {
        return mean;
    }

    public void setMean(float mean) {
        this.mean = mean;
    }

    public int getFemale() {
        return female;
    }

    public void setFemale(int female) {
        this.female = female;
    }
}

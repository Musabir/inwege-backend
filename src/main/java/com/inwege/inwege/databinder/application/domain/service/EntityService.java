package com.inwege.inwege.databinder.application.domain.service;

import com.inwege.inwege.common.application.dto.ResponseDTO;
import com.inwege.inwege.common.application.dto.ResponseStatus;
import com.inwege.inwege.common.application.exception.JobEntityNotFoundException;
import com.inwege.inwege.common.application.exception.SalaryEntityAverageNotFoundException;
import com.inwege.inwege.common.application.exception.SalaryEntityNotFoundException;
import com.inwege.inwege.databinder.application.domain.model.JobEntity;
import com.inwege.inwege.databinder.application.domain.model.SalaryEntity;
import com.inwege.inwege.databinder.application.domain.model.SalaryEntityAverage;
import com.inwege.inwege.databinder.application.domain.repository.JobEntityRepository;
import com.inwege.inwege.databinder.application.domain.repository.SalaryEntityAverageRepository;
import com.inwege.inwege.databinder.application.domain.repository.SalaryEntityRepository;
import com.inwege.inwege.databinder.application.dto.AverageMeanDTO;
import com.inwege.inwege.databinder.application.dto.JobAndSalaryEntitiesDTO;
import com.inwege.inwege.databinder.application.dto.JobEntityDTO;
import com.inwege.inwege.databinder.application.dto.JobNameDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class EntityService {

    @Autowired
    SalaryEntityRepository salaryEntityRepository;

    @Autowired
    JobEntityRepository jobEntityRepository;

    @Autowired
    SalaryEntityAverageRepository salaryEntityAverageRepository;

    @Autowired
    ResourceLoader resourceLoader;

    public List<String> findAllRegions() {
        List<String> regions = findAllDistictRegions();
        Collections.sort(regions);
        return regions;
    }

    private List<String> findAllDistictRegions() {
        List<String> regions = salaryEntityRepository.findAllDistictRegions();
        return regions;
    }

    public List<JobNameDTO> findJobNamesByRegion(String region, String lang) throws SalaryEntityNotFoundException, JobEntityNotFoundException {
        List<SalaryEntity> salaryEntities = findIscosByRegion(region);
        List<JobNameDTO> jobNameDTOS = getJobNamesBySalaryEntitiesAndLang(salaryEntities, lang);
        return jobNameDTOS;

    }

    private List<SalaryEntity> findIscosByRegion(String region) throws SalaryEntityNotFoundException {
        List<SalaryEntity> salaryEntities = salaryEntityRepository.findIscosByRegion(region);
        if (salaryEntities.isEmpty())
            throw new SalaryEntityNotFoundException("No any salary entity found with the given region: " + region);
        return salaryEntities;

    }

    private List<JobNameDTO> getJobNamesBySalaryEntitiesAndLang(List<SalaryEntity> salaryEntities, String lang) throws JobEntityNotFoundException {
        List<JobNameDTO> jobNameDTOS = new ArrayList<>();
        for (SalaryEntity salaryEntity : salaryEntities) {
            JobNameDTO jobName = getJobNameByLevel(salaryEntity, lang);
            if (jobName != null) {
                jobName.setIscoValid(salaryEntity.getIscoValid());
                jobNameDTOS.add(jobName);
            }
        }
        Collections.sort(jobNameDTOS,
                (o1, o2) -> o1.getName().compareTo(o2.getName()));
        return jobNameDTOS;
    }

    public List<AverageMeanDTO> findAvgMean(Long id) throws SalaryEntityNotFoundException {
        List<SalaryEntity> salaryEntities = findSalaryEntitiesByOccupation(id);
        return getAverageMeanBySalaryEntities(salaryEntities);
    }

    private List<SalaryEntity> findSalaryEntitiesByIscoValid(String isco) throws SalaryEntityNotFoundException {
        List<SalaryEntity> salaryEntities = salaryEntityRepository.findSalaryEntitiesByIscoValid(isco);
        if (salaryEntities.isEmpty())
            throw new SalaryEntityNotFoundException("No any salary entity found with the given isco: " + isco);
        return salaryEntities;
    }

    private SalaryEntity findSalaryEntityById(Long id) throws SalaryEntityNotFoundException {
        SalaryEntity salaryEntity = salaryEntityRepository.findById(id).orElse(null);
        if (salaryEntity == null)
            throw new SalaryEntityNotFoundException("No salary entity found with id: " + id);
        return salaryEntity;
    }

    private List<SalaryEntity> findSalaryEntitiesByOccupation(Long id) throws SalaryEntityNotFoundException {
        SalaryEntity salaryEntity = findSalaryEntityById(id);
        List<SalaryEntity> salaryEntities =
                salaryEntityRepository.findSalaryEntitiesByIscoValidAndOneDigitAndTwoDigitAndThreeDigitAndFourDigit(
                        salaryEntity.getIscoValid(), salaryEntity.getOneDigit(), salaryEntity.getTwoDigit(),
                        salaryEntity.getThreeDigit(), salaryEntity.getFourDigit());
        if (salaryEntities.isEmpty())
            throw new SalaryEntityNotFoundException("No any salary entity found with the given isco: " + salaryEntity.getIscoValid());
        return salaryEntities;
    }

    private List<SalaryEntity> findSalaryEntitiesByIscoValidAndGender(String isco, String gender) throws SalaryEntityNotFoundException {
        List<SalaryEntity> salaryEntities = salaryEntityRepository.findSalaryEntitiesByIscoValidAndFemale(isco, gender);
        if (salaryEntities.isEmpty())
            throw new SalaryEntityNotFoundException("No any salary entity found with the given isco: " + isco + " and gender: " + gender);
        return salaryEntities;
    }

    public List<AverageMeanDTO> getAverageMeanBySalaryEntities(List<SalaryEntity> salaryEntities) {
        double[] result = calculateSumAndCounter(salaryEntities);
        double femaleAverage = result[0];
        double maleAverage = result[1];

        List<AverageMeanDTO> averageMeanDTOS = new ArrayList<>();
        averageMeanDTOS.add(new AverageMeanDTO(femaleAverage, 0));
        averageMeanDTOS.add(new AverageMeanDTO(maleAverage, 1));
        return averageMeanDTOS;
    }

    private double[] calculateSumAndCounter(List<SalaryEntity> salaryEntities) {
        double[] result = new double[2];
        double[] sum = new double[2];
        int[] counter = new int[2];

        for (SalaryEntity salaryEntity : salaryEntities) {
            try {
                if (Integer.parseInt(salaryEntity.getFemale()) == 0) {
                    sum[0] += Double.parseDouble(salaryEntity.getMean());
                    counter[0]++;
                } else if (Integer.parseInt(salaryEntity.getFemale()) == 1) {
                    sum[1] += Double.parseDouble(salaryEntity.getMean());
                    counter[1]++;
                }
            } catch (NumberFormatException e) {
            }
        }
        try {
            result[0] = Math.round(sum[0] / counter[0]);
            result[1] = Math.round(sum[1] / counter[1]);
        } catch (ArithmeticException e) {
        }
        return result;
    }

    public SalaryEntity findSalaryEntryByRegionAndIsco(String region, String iscoValid) throws SalaryEntityNotFoundException {
        List<SalaryEntity> salaryEntities = findSalaryEntriesByRegionAndIsco(region, iscoValid);
        if (!salaryEntities.isEmpty())
            return salaryEntities.get(0);
        return null;
    }

    private List<SalaryEntity> findSalaryEntriesByRegionAndIsco(String region, String iscoValid) throws SalaryEntityNotFoundException {
        List<SalaryEntity> salaryEntities = salaryEntityRepository.findSalaryEntriesByRegionAndIsco(region, iscoValid);
        return salaryEntities;

    }

    public JobAndSalaryEntitiesDTO findJobEntityByCode(String isco, String region, String code, String lang) throws JobEntityNotFoundException, SalaryEntityNotFoundException {
        List<JobEntity> jobEntities = findJobEntityByCode(code);
        JobEntity je = jobEntities.get(0);
        JobEntityDTO jobEntityDTO = getJobEntityDTOByLang(je, lang);
        List<SalaryEntity> salaryEntities = findSalaryEntriesByRegionAndIsco(region, isco);
        if (salaryEntities.isEmpty())
            salaryEntities = findAverageByOccupation(isco);
        JobAndSalaryEntitiesDTO js = new JobAndSalaryEntitiesDTO(salaryEntities, jobEntityDTO);
        return js;
    }

    public SalaryEntityAverage findSalaryEntityAverageByRegion(String region) throws SalaryEntityAverageNotFoundException {
        SalaryEntityAverage salaryEntityAverageByRegion = salaryEntityAverageRepository.findSalaryEntityAverageByRegion(region);
        if(salaryEntityAverageByRegion==null)
            throw new SalaryEntityAverageNotFoundException("No such region found: "+ region);
        return salaryEntityAverageByRegion;
    }

    private List<SalaryEntity> findAverageByOccupation(String isco) throws SalaryEntityNotFoundException {
        List<SalaryEntity> result = new ArrayList<>();
        result.add(calculateAverageSalaryEntity(findSalaryEntitiesByIscoValidAndGender(isco, "0"), isco, "0"));
        result.add(calculateAverageSalaryEntity(findSalaryEntitiesByIscoValidAndGender(isco, "1"), isco, "1"));
        return result;
    }

    private SalaryEntity calculateAverageSalaryEntity(List<SalaryEntity> salaryEntities, String isco, String gender) {
        SalaryEntity salaryEntity = salaryEntities.get(0);
        salaryEntities.remove(0);
        int count = 2;
        salaryEntity.setFemale(gender);
        salaryEntity.setIscoValid(isco);
        salaryEntity.setRegion("All");
        for (SalaryEntity se : salaryEntities) {
            System.out.println("Sizee " + salaryEntity.getP1() + "  " + se.getP1());

            salaryEntity.setP1(Math.round((Double.parseDouble(se.getP1()) + Double.parseDouble(salaryEntity.getP1())) / count) + "");
            salaryEntity.setP2(Math.round((Double.parseDouble(se.getP2()) + Double.parseDouble(salaryEntity.getP2())) / count) + "");
            salaryEntity.setP3(Math.round((Double.parseDouble(se.getP3()) + Double.parseDouble(salaryEntity.getP3())) / count) + "");
            salaryEntity.setP4(Math.round((Double.parseDouble(se.getP4()) + Double.parseDouble(salaryEntity.getP4())) / count) + "");
            salaryEntity.setP6(Math.round((Double.parseDouble(se.getP6()) + Double.parseDouble(salaryEntity.getP6())) / count) + "");
            salaryEntity.setP7(Math.round((Double.parseDouble(se.getP7()) + Double.parseDouble(salaryEntity.getP7())) / count) + "");
            salaryEntity.setP8(Math.round((Double.parseDouble(se.getP8()) + Double.parseDouble(salaryEntity.getP8())) / count) + "");
            salaryEntity.setP9(Math.round((Double.parseDouble(se.getP9()) + Double.parseDouble(salaryEntity.getP9())) / count) + "");
            salaryEntity.setN((Math.round(Double.parseDouble(se.getN()) + Double.parseDouble(salaryEntity.getN())) / count) + "");
            salaryEntity.setMean(Math.round((Double.parseDouble(se.getMean()) + Double.parseDouble(salaryEntity.getMean())) / count) + "");
            salaryEntity.setMedian(Math.round((Double.parseDouble(se.getMedian()) + Double.parseDouble(salaryEntity.getMedian())) / count) + "");
            salaryEntity.setMaxVal(Math.round((Double.parseDouble(se.getMaxVal()) + Double.parseDouble(salaryEntity.getMaxVal())) / count) + "");
            salaryEntity.setMinVal(Math.round((Double.parseDouble(se.getMinVal()) + Double.parseDouble(salaryEntity.getMinVal())) / count) + "");
        }
        return salaryEntity;
    }

    private List<JobEntity> findJobEntityByCode(String code) throws JobEntityNotFoundException {
        List<JobEntity> jobEntities = jobEntityRepository.findJobEntityByCode(code);
        if (jobEntities.isEmpty())
            throw new JobEntityNotFoundException("No job entity found with the given code: " + code);
        return jobEntities;
    }


    private JobNameDTO getJobNameDTO(JobEntity jobEntity, String lang) {
        JobNameDTO jobNameDTO = new JobNameDTO();
        jobNameDTO.setCode(jobEntity.getCode());
        jobNameDTO.setId(jobEntity.getId());
        switch (lang) {
            case "en":
                jobNameDTO.setName(jobEntity.getName_en());
                break;
            case "ru":
                jobNameDTO.setName(jobEntity.getName_ru());
                break;
            default:
                jobNameDTO.setName(jobEntity.getName_ee());
                break;
        }
        return jobNameDTO;
    }

    private JobNameDTO getJobNameByLevel(SalaryEntity salaryEntity, String lang) throws JobEntityNotFoundException {
        String code = salaryEntity.getIscoValid();
        if (salaryEntity.getOneDigit().equals("1"))
            code = code.substring(0, 1);
        else if (salaryEntity.getTwoDigit().equals("1"))
            code = code.substring(0, 2);
        else if (salaryEntity.getThreeDigit().equals("1"))
            code = code.substring(0, 3);
        else if (salaryEntity.getFourDigit().equals("1"))
            code = code.substring(0, 4);
        List<JobEntity> jobEntities = findJobEntityByCode(code);
        return getJobNameDTO(jobEntities.get(0), lang);
    }

    private JobEntityDTO getJobEntityDTOByLang(JobEntity je, String lang) {
        JobEntityDTO jobEntityDTO = new JobEntityDTO();
        jobEntityDTO.setId(je.getId());
        jobEntityDTO.setCode(je.getCode());
        switch (lang) {
            case "en":
                jobEntityDTO.setName(je.getName_en());
                jobEntityDTO.setDescription(je.getDescription_en());
                jobEntityDTO.setBelong_group(je.getBelong_group_en());
                jobEntityDTO.setNot_belong_group(je.getNot_belong_group_en());
                break;
            case "ru":
                jobEntityDTO.setName(je.getName_ru());
                jobEntityDTO.setDescription(je.getDescription_ru());
                jobEntityDTO.setBelong_group(je.getBelong_group_ru());
                jobEntityDTO.setNot_belong_group(je.getNot_belong_group_ru());
                break;
            default:
                jobEntityDTO.setName(je.getName_ee());
                jobEntityDTO.setDescription(je.getDescription_ee());
                jobEntityDTO.setBelong_group(je.getBelong_group_ee());
                jobEntityDTO.setNot_belong_group(je.getNot_belong_group_ee());
                break;
        }
        return jobEntityDTO;
    }
}

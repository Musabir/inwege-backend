package com.inwege.inwege.databinder.application.domain.repository;

import com.inwege.inwege.databinder.application.domain.model.SalaryEntity;
import com.inwege.inwege.databinder.application.domain.model.SalaryEntityAverage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalaryEntityAverageRepository extends JpaRepository<SalaryEntityAverage, Long> {

    SalaryEntityAverage findSalaryEntityAverageByRegion(String region);


}

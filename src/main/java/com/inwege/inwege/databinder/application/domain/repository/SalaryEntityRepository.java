package com.inwege.inwege.databinder.application.domain.repository;

import com.inwege.inwege.databinder.application.domain.model.SalaryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface SalaryEntityRepository extends JpaRepository<SalaryEntity, Long> {

    @Query(value = "SELECT DISTINCT REGION FROM Salary_Entity", nativeQuery = true)
    List<String> findAllDistictRegions();

    @Query(value = "SELECT DISTINCT on (isco_valid) * FROM salary_entity entity WHERE entity.region = ?1",nativeQuery = true)
    List<SalaryEntity> findIscosByRegion(String region);

    @Query("SELECT entity FROM SalaryEntity entity WHERE entity.region = ?1 AND entity.iscoValid = ?2")
    List<SalaryEntity> findSalaryEntriesByRegionAndIsco(String region, String isco);

    List<SalaryEntity> findSalaryEntitiesByIscoValid(String iscoValid);

    List<SalaryEntity> findSalaryEntitiesByIscoValidAndOneDigitAndTwoDigitAndThreeDigitAndFourDigit(String iscoValid, String oneDigit, String twoDigit, String threeDigit, String fourDigit);

    List<SalaryEntity> findSalaryEntitiesByIscoValidAndFemale(String iscoValid, String female);

}

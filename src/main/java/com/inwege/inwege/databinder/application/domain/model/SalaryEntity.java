package com.inwege.inwege.databinder.application.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.MappingIterator;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;

@Data
@Entity
public class SalaryEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String region, female, iscoValid, n, mean, median, sd, minVal, maxVal, p1, p2, p3, p4, p6, p7, p8, p9;

    @JsonIgnore
    private String oneDigit, twoDigit, threeDigit, fourDigit;

    public SalaryEntity(ArrayList<String> columns) {
        this.region = columns.get(0);
        this.female = columns.get(1);
        this.iscoValid = columns.get(2);
        this.n = columns.get(3);
        this.mean = columns.get(4);
        this.median = columns.get(5);
        this.sd = columns.get(6);
        this.minVal = columns.get(7);
        this.maxVal = columns.get(8);
        this.p1 = columns.get(9);
        this.p2 = columns.get(10);
        this.p3 = columns.get(11);
        this.p4 = columns.get(12);
        this.p6 = columns.get(13);
        this.p7 = columns.get(14);
        this.p8 = columns.get(15);
        this.p9 = columns.get(16);
        this.oneDigit = columns.get(17);
        this.twoDigit = columns.get(18);
        this.threeDigit = columns.get(19);
        this.fourDigit = columns.get(20);
    }

    public SalaryEntity() {

    }

}

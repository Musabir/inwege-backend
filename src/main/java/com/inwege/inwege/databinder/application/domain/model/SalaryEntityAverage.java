package com.inwege.inwege.databinder.application.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;

@Data
@Entity
public class SalaryEntityAverage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private Double maleAverage, femaleAverage;
    private String region;

    public SalaryEntityAverage(){ }
}
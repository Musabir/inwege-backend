package com.inwege.inwege.databinder.application.domain.rest;

import com.inwege.inwege.common.application.dto.ResponseDTO;
import com.inwege.inwege.common.application.dto.ResponseStatus;
import com.inwege.inwege.common.application.exception.JobEntityNotFoundException;
import com.inwege.inwege.common.application.exception.SalaryEntityAverageNotFoundException;
import com.inwege.inwege.common.application.exception.SalaryEntityNotFoundException;
import com.inwege.inwege.databinder.application.domain.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@CrossOrigin
@RestController
@RequestMapping("/api")
public class MainController {

    @Autowired
    EntityService entityService;

    @GetMapping("/regions")
    public ResponseDTO<?> findAllRegions() {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.OK.value(), "List of all iscos",
                entityService.findAllRegions());

    }

    @GetMapping("/average")
    public ResponseDTO<?> findAverageByRegion(@RequestParam("region") String region) throws SalaryEntityAverageNotFoundException {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.OK.value(),
                "Average by region: " + region, entityService.findSalaryEntityAverageByRegion(region));
    }

    @GetMapping("/salary-entities")
    public ResponseDTO<?> findSalaryEntryByRegionAndIsco(@RequestParam("region") String region, @RequestParam("isco") String isco) throws SalaryEntityNotFoundException {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.OK.value(), "All filtered entries",
                entityService.findSalaryEntryByRegionAndIsco(region, isco));
    }

    @GetMapping("/jobs/{id}/average")
    public ResponseDTO<?> findAverageMeanByIsco(@PathVariable Long id) throws SalaryEntityNotFoundException {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.OK.value(),
                "Mean by salary entity ", entityService.findAvgMean(id));

    }

    @GetMapping("/jobs/names")
    public ResponseDTO<?> findJobNamesByRegion(@RequestParam("region") String region, @RequestParam("lang") String lang) throws SalaryEntityNotFoundException, JobEntityNotFoundException {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.OK.value(),
                "All iscos of region: " + region, entityService.findJobNamesByRegion(region, lang));

    }

    @GetMapping("/jobs")
    public ResponseDTO<?> findJobEntityByCode(@RequestParam("region") String region, @RequestParam("isco") String isco,
                                              @RequestParam("code") String code, @RequestParam("lang") String lang) throws SalaryEntityNotFoundException, JobEntityNotFoundException {
        return new ResponseDTO<>(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.OK.value(),
                "The entry by code: " + code, entityService.findJobEntityByCode(isco, region, code, lang));

    }

    @ExceptionHandler(SalaryEntityNotFoundException.class)
    public ResponseDTO handleSalaryEntityNotFoundException(SalaryEntityNotFoundException ex) {
        return new ResponseDTO(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), new ArrayList<>());
    }

    @ExceptionHandler(JobEntityNotFoundException.class)
    public ResponseDTO handleJobEntityNotFoundException(JobEntityNotFoundException ex) {
        return new ResponseDTO(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), new ArrayList<>());
    }

    @ExceptionHandler(SalaryEntityAverageNotFoundException.class)
    public ResponseDTO handleSalaryEntityAverageNotFoundException(SalaryEntityAverageNotFoundException ex) {
        return new ResponseDTO(ResponseStatus.SUCCESS.name().toLowerCase(), HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), new ArrayList<>());
    }


}

package com.inwege.inwege.databinder.application.dto;

import com.inwege.inwege.databinder.application.domain.model.SalaryEntity;
import lombok.Data;

import java.util.List;

@Data
public class JobAndSalaryEntitiesDTO {
    List<SalaryEntity> salaryEntities;
    JobEntityDTO jobEntity;

    public JobAndSalaryEntitiesDTO(List<SalaryEntity> salaryEntities, JobEntityDTO jobEntity) {
        this.salaryEntities = salaryEntities;
        this.jobEntity = jobEntity;
    }

    public JobAndSalaryEntitiesDTO() {
    }
}

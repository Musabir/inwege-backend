package com.inwege.inwege.databinder.application.dto;

import lombok.Data;

@Data
public class JobNameDTO {
    Long id;
    String name;
    String code;
    String iscoValid;

    public JobNameDTO(Long id, String name,String iscoValid){
        this.id = id;
        this.name = name;
        this.iscoValid = iscoValid;
    }

    public JobNameDTO(){}
}


package com.inwege.inwege.databinder.application.domain.repository;

import com.inwege.inwege.databinder.application.domain.model.JobEntity;
import com.inwege.inwege.databinder.application.domain.model.SalaryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface JobEntityRepository extends JpaRepository<JobEntity, Long> {

    @Query(value = "SELECT DISTINCT name_ee FROM Job_Entity", nativeQuery = true)
    List<String> findAllNameEe();

    @Query(value = "SELECT DISTINCT name_en FROM Job_Entity", nativeQuery = true)
    List<String> findAllNameEn();

    @Query(value = "SELECT DISTINCT  name_ru FROM Job_Entity", nativeQuery = true)
    List<String> findAllNameRu();


    List<JobEntity> findJobEntityByCode(String code);


}

package com.inwege.inwege;

import com.inwege.inwege.common.application.defaults.Util;
import com.inwege.inwege.common.application.exception.SalaryEntityNotFoundException;
import com.inwege.inwege.databinder.application.domain.model.JobEntity;
import com.inwege.inwege.databinder.application.domain.model.SalaryEntity;
import com.inwege.inwege.databinder.application.domain.model.SalaryEntityAverage;
import com.inwege.inwege.databinder.application.domain.repository.JobEntityRepository;
import com.inwege.inwege.databinder.application.domain.repository.SalaryEntityAverageRepository;
import com.inwege.inwege.databinder.application.domain.repository.SalaryEntityRepository;
import com.inwege.inwege.databinder.application.domain.service.EntityService;
import com.inwege.inwege.databinder.application.dto.AverageMeanDTO;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class LoadCVS implements CommandLineRunner {

    @Autowired
    SalaryEntityRepository salaryEntityRepository;

    @Autowired
    JobEntityRepository jobEntityRepository;

    @Autowired
    SalaryEntityAverageRepository salaryEntityAverageRepository;

    @Autowired
    ResourceLoader resourceLoader;

    DataFormatter dataFormatter;

    @Autowired
    EntityService entityService;

    public void loadSalaryEntityFromCVS() throws IOException, InvalidFormatException {
        Sheet sheet = getSheetFromFile(Util.salaryEntityFileURL);
        List<SalaryEntity> salaryEntities = parseSalaryEntitiesSheet(sheet);
        salaryEntityRepository.saveAll(salaryEntities);
        saveSalaryEntityAverage();
    }

    private void saveSalaryEntityAverage() {
        List<String> regions = entityService.findAllRegions();
        double maleAverage = 0, femaleAverage = 0;
        for (String region : regions) {
            List<SalaryEntity> salaryEntities = salaryEntityRepository.findIscosByRegion(region);
            List<AverageMeanDTO> averageMeanDTOS = entityService.getAverageMeanBySalaryEntities(salaryEntities);
            if (!averageMeanDTOS.isEmpty()) {
                maleAverage += averageMeanDTOS.get(0).getMean();
                femaleAverage += averageMeanDTOS.get(1).getMean();
                SalaryEntityAverage salaryEntityAverage = parseAverageMeanDTOs(averageMeanDTOS);
                salaryEntityAverage.setRegion(region);
                salaryEntityAverageRepository.save(salaryEntityAverage);
            }
        }
        if (regions.size() > 0)
            saveAverageOfAllSalaryEntities(maleAverage, femaleAverage, regions.size());
    }

    private SalaryEntityAverage parseAverageMeanDTOs(List<AverageMeanDTO> averageMeanDTOS) {
        SalaryEntityAverage salaryEntityAverage = new SalaryEntityAverage();
        salaryEntityAverage.setMaleAverage(averageMeanDTOS.get(0).getMean());
        salaryEntityAverage.setFemaleAverage(averageMeanDTOS.get(1).getMean());
        return salaryEntityAverage;
    }

    private void saveAverageOfAllSalaryEntities(double maleAverage, double femaleAverage, int size) {
        SalaryEntityAverage averageAll = new SalaryEntityAverage();
        averageAll.setRegion("All");
        averageAll.setMaleAverage(Math.round(maleAverage / size) * 1.0);
        averageAll.setFemaleAverage(Math.round(femaleAverage / size) * 1.0);
        salaryEntityAverageRepository.save(averageAll);
    }

    private List<SalaryEntity> parseSalaryEntitiesSheet(Sheet sheet) {
        ArrayList<SalaryEntity> salaryEntities = new ArrayList<>();
        ArrayList<String> salaryEntityArray = new ArrayList<>();
        for (int i = 2; i <= sheet.getLastRowNum(); i++) {
            salaryEntityArray = parseSalaryEntityRow(sheet.getRow(i));
            if (salaryEntityArray.size() > 3) {
                salaryEntities.add(new SalaryEntity(salaryEntityArray));
            }
        }

        return salaryEntities;
    }

    private ArrayList<String> parseSalaryEntityRow(Row row) {
        ArrayList<String> salaryEntityArray = new ArrayList<>();
        for (int cn = 0; cn < 21; cn++) {
            Cell cell = row.getCell(cn);
            if (cell != null)
                salaryEntityArray.add(dataFormatter.formatCellValue(cell));
            else {
                salaryEntityArray.clear();
                break;
            }
        }
        return salaryEntityArray;
    }

    public void loadJobEntityFromCVS() throws IOException, InvalidFormatException {
        Sheet sheet = getSheetFromFile(Util.jobEntityFileURL);
        ArrayList<JobEntity> jobEntities = parseJobEntitiesSheet(sheet);

        jobEntityRepository.saveAll(jobEntities);
    }

    private ArrayList<JobEntity> parseJobEntitiesSheet(Sheet sheet) {
        ArrayList<JobEntity> jobEntities = new ArrayList<>();
        ArrayList<String> jobEntityArray = new ArrayList<>();
        for (int i = 3; i <= sheet.getLastRowNum(); i++) {
            jobEntityArray = parseJobEntityRow(sheet.getRow(i));
            if (jobEntityArray.size() > 3)
                jobEntities.add(new JobEntity(jobEntityArray));
        }
        return jobEntities;
    }

    private ArrayList<String> parseJobEntityRow(Row row) {
        ArrayList<String> jobEntityArray = new ArrayList<>();
        for (int cn = 0; cn < 15; cn++) {
            Cell cell = row.getCell(cn);
            if (cell == null)
                jobEntityArray.add(null);
            else
                jobEntityArray.add(dataFormatter.formatCellValue(cell));
        }
        return jobEntityArray;
    }

    private Sheet getSheetFromFile(String url) throws IOException, InvalidFormatException {
        InputStream in = new URL(url).openStream();
        Workbook workbook = WorkbookFactory.create(in);
        Sheet sheet = workbook.getSheetAt(0);
        return sheet;
    }

    @Override
    public void run(String... args) throws Exception {
        dataFormatter = new DataFormatter();
        if (salaryEntityRepository.findAll().size() == 0) {
            loadSalaryEntityFromCVS();
        }
        if (jobEntityRepository.findAllNameEe().size() == 0)
            loadJobEntityFromCVS();

    }
}
